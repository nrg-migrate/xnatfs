package org.xnat.xnatfs.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestPathUtils {

  @Test
  public void testRoot () {
    assertEquals ( "dir/sample", PathUtils.root ( "dir/sample.gif" ) );
    assertEquals ( "dir/sample", PathUtils.root ( "dir/sample" ) );
  }

  @Test
  public void testTail () {
    assertEquals ( "sample", PathUtils.tail ( "grand/parent/sample" ) );
    assertEquals ( "sample", PathUtils.tail ( "grand/parent/sample/" ) );
    assertEquals ( "something", PathUtils.tail ( "something" ) );
  }

  @Test
  public void testExtention () {
    assertEquals ( ".gif", PathUtils.extension ( "dir/sample_12.gif" ) );
    assertEquals ( "", PathUtils.extension ( "dir/sample_12" ) );
  }

  @Test
  public void testDirname () {
    assertEquals ( "dir", PathUtils.dirname ( "dir/sample_12" ) );
    assertEquals ( "dir", PathUtils.dirname ( "dir" ) );
    assertEquals ( "/", PathUtils.dirname ( "/dir" ) );
    assertEquals ( "/grand/dir", PathUtils.dirname ( "/grand/dir/sample_12" ) );
    assertEquals ( "/grand", PathUtils.dirname ( "/grand/dir/" ) );
  }

}
