package org.xnat.xnatfs.util;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.xnat.xnatfs.asset.IStringAsset;
import org.xnat.xnatfs.util.JSONUtils;

public class TestJSONUtils {
  private static final String EXPERIMENTS_JSON = "{\"ResultSet\":{\"Result\":[{\"id\":\"Demo_E00001\",\"element_name\":\"xnat:mrSessionData\",\"project\":\"Demo\",\"projects\":\"Demo\",\"date\":\"2006-12-14\",\"label\":\"SampleID\",\"insert_date\":\"2010-01-12 11:11:52.0\",\"assessments\":\"0\"}], \"totalRecords\": \"1\",\"title\": \"Experiments\"}}";

  @Before
  public void setUp () throws Exception {
  }

  @Test
  public void shouldParseJSONString () throws Exception {
    JSONObject obj = JSONUtils.parseJSON ( EXPERIMENTS_JSON ).getJSONObject ( "ResultSet" );
    assertEquals ( "1", obj.getString ( "totalRecords" ) );
  }

  @Test
  public void shouldParseJSONAsset () throws Exception {
    IStringAsset asset = mock ( IStringAsset.class );
    when ( asset.getContent () ).thenReturn ( EXPERIMENTS_JSON );

    JSONObject obj = JSONUtils.parseJSON ( asset ).getJSONObject ( "ResultSet" );
    assertEquals ( "1", obj.getString ( "totalRecords" ) );
  }
}
