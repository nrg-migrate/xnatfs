package org.xnat.xnatfs.asset;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class StreamUtils {

  public static InputStream toStream ( String data ) {
    return new ByteArrayInputStream ( data.getBytes () );
  }
}
