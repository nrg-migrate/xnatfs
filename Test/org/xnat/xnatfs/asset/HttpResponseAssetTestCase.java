package org.xnat.xnatfs.asset;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.junit.Before;

public class HttpResponseAssetTestCase {
  protected static final String SAMPLE_STRING = "hello world\ngoodbye moon";

  protected HttpEntity entity;
  protected HttpResponse response;
  protected StatusLine statusLine;

  @Before
  public void setUp () throws Exception {
    entity = mock ( HttpEntity.class );

    statusLine = mock ( StatusLine.class );
    when ( statusLine.getStatusCode () ).thenReturn ( 200 );

    response = mock ( HttpResponse.class );
    when ( response.getEntity () ).thenReturn ( entity );
    when ( response.getStatusLine () ).thenReturn ( statusLine );
  }

}
