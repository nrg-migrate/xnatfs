package org.xnat.xnatfs.asset;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;

public class TestHttpResponseStreamAsset extends HttpResponseAssetTestCase {
  private HttpResponseStreamAsset asset;
  private InputStream stream;

  @Before
  public void setUp () throws Exception {
    super.setUp ();
    stream = StreamUtils.toStream ( SAMPLE_STRING );
  }

  @Test
  public void shouldProvideInputStream () throws Exception {
    when ( entity.getContent () ).thenReturn ( stream );
    asset = new HttpResponseStreamAsset ( response );

    assertEquals ( stream, asset.getContent () );
  }

  @Test
  public void shouldConsumeOnClose () throws Exception {
    when ( entity.getContent () ).thenReturn ( stream );
    asset = new HttpResponseStreamAsset ( response );

    asset.close ();

    verify ( entity ).consumeContent ();
  }

  @Test
  public void shouldPropogateEntityError () throws Exception {
    when ( entity.getContent () ).thenThrow ( new IOException ( "fake exception" ) );
    asset = new HttpResponseStreamAsset ( response );

    try {
      asset.getContent ();
      fail ( "Expected exception" );
    } catch ( IOException e ) {
      // expected
    }
  }

}
