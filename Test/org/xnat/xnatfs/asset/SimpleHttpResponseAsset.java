package org.xnat.xnatfs.asset;

import java.io.IOException;

import org.apache.http.HttpResponse;

public class SimpleHttpResponseAsset extends HttpResponseAsset {

  public SimpleHttpResponseAsset ( HttpResponse response ) {
    super ( response );
  }

  @Override
  public void close () throws IOException {

  }
}
