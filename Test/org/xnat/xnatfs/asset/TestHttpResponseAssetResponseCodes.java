package org.xnat.xnatfs.asset;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.junit.Before;
import org.junit.Test;

public class TestHttpResponseAssetResponseCodes {
  private HttpResponse response;
  private HttpResponseAsset asset;
  private StatusLine statusLine;

  @Before
  public void setUp () throws Exception {
    statusLine = mock ( StatusLine.class );
    response = mock ( HttpResponse.class );
    when ( response.getStatusLine () ).thenReturn ( statusLine );
    asset = new SimpleHttpResponseAsset ( response );
  }

  @Test
  public void shouldNotAuthoriseFor401 () {
    setStatus ( 401 );
    asset = new SimpleHttpResponseAsset ( response );
    assertFalse ( asset.authorised () );
  }

  @Test
  public void shouldNotAuthoriseFor403 () {
    setStatus ( 403 );
    assertFalse ( asset.authorised () );
  }

  @Test
  public void shouldAuthoriseFor200 () {
    setStatus ( 200 );
    assertTrue ( asset.authorised () );
  }

  @Test
  public void shouldNotFindFor404 () {
    setStatus ( 404 );
    assertFalse ( asset.found () );
  }

  @Test
  public void shouldFindFor200 () {
    setStatus ( 200 );
    assertTrue ( asset.found () );
  }

  private void setStatus ( int statusCode ) {
    when ( statusLine.getStatusCode () ).thenReturn ( statusCode );
  }

}
