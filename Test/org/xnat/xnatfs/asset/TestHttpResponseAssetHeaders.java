package org.xnat.xnatfs.asset;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.junit.Before;
import org.junit.Test;

public class TestHttpResponseAssetHeaders {
  private HttpResponse response;
  private HttpResponseAsset asset;
  private HttpEntity entity;

  @Before
  public void setUp () throws Exception {
    response = mock ( HttpResponse.class );

    entity = mock ( HttpEntity.class );
    when ( response.getEntity () ).thenReturn ( entity );

    asset = new SimpleHttpResponseAsset ( response );
  }

  @Test
  public void shouldProvideContentLength () {
    when ( entity.getContentLength () ).thenReturn ( 128L );

    assertEquals ( 128L, asset.getContentLength () );
  }

  @Test
  public void shouldHandleNoEntity () {
    when ( response.getEntity () ).thenReturn ( null );
    
    assertEquals ( -1L, asset.getContentLength () );
  }

}
