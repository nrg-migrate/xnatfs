package org.xnat.xnatfs.event;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.xnat.xnatfs.webdav.VirtualResource;

import com.bradmcevoy.http.Request;
import com.bradmcevoy.http.Response;

public class TestResourceClosingEventListener {
  private Request request;
  private Response response;
  private VirtualResource resource;
  private ResourceClosingEventListener listener;

  @Before
  public void setUp () throws Exception {
    request = mock ( Request.class );
    response = mock ( Response.class );
    resource = mock ( VirtualResource.class );

    listener = new ResourceClosingEventListener ();
  }

  @Test
  public void shouldCloseResource () {
    listener.onProcessResourceFinish ( request, response, resource, 20L );

    verify ( resource ).close ();
  }

}
