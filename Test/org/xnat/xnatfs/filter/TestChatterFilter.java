package org.xnat.xnatfs.filter;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import com.bradmcevoy.http.FilterChain;
import com.bradmcevoy.http.Request;
import com.bradmcevoy.http.Response;

public class TestChatterFilter {
  private ChatterFilter filter;
  private Request request;
  private Response response;
  private FilterChain chain;

  @Before
  public void setUp () throws Exception {
    request = mock ( Request.class );
    response = mock ( Response.class );
    chain = mock ( FilterChain.class );

    filter = new ChatterFilter ();
  }

  @Test
  public void shouldAllowRoot () {
    assertAllowed ( "/" );
  }

  @Test
  public void shouldAllowNormalRequest () {
    assertAllowed ( "/Projects/Demo/Subjects/" );
  }

  @Test
  public void should404favicon () {
    when ( request.getAbsolutePath () ).thenReturn ( "/favicon.ico" );
    filter.process ( chain, request, response );
    verify ( chain, never () ).process ( request, response );
    verify ( response ).setStatus ( Response.Status.SC_NOT_FOUND );
  }

  private void assertAllowed ( String path ) {
    when ( request.getAbsolutePath () ).thenReturn ( path );
    filter.process ( chain, request, response );
    verify ( chain ).process ( request, response );
  }
}
