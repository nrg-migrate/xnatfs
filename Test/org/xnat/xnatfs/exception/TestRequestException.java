package org.xnat.xnatfs.exception;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestRequestException {

  private Exception cause;
  private RequestException e;

  @Before
  public void setUp () throws Exception {
    cause = new Exception ();
  }

  @Test
  public void testRequestExceptionException () {
    e = new RequestException ( cause );
    assertEquals ( cause, e.getCause () );
  }

  @Test
  public void testRequestExceptionStringException () {
    e = new RequestException ( "Some message", cause );
    assertEquals ( "Some message", e.getMessage () );
    assertEquals ( cause, e.getCause () );
  }

  @Test
  public void testRequestExceptionString () {
    e = new RequestException ( "Some message" );
    assertEquals ( "Some message", e.getMessage () );
  }

}
