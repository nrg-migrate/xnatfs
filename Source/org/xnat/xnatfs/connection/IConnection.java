package org.xnat.xnatfs.connection;

import org.apache.http.HttpResponse;
import org.xnat.xnatfs.exception.RequestException;

import com.bradmcevoy.http.Auth;

public interface IConnection {

  public HttpResponse doGet ( String url, Auth credentials ) throws RequestException;

  public HttpResponse doHead ( String url, Auth auth ) throws RequestException;

}
