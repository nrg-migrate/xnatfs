package org.xnat.xnatfs.connection;

public class ConnectionFactory {
  static private IConnection connection;

  static public IConnection getConnection () {
    if ( connection == null ) {
      connection = new HttpConnection ();
    }
    return connection;
  }

  static public void setConnection ( IConnection c ) {
    connection = c;
  }
}
