package org.xnat.xnatfs.connection;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.http.HttpResponse;
import org.xnat.xnatfs.exception.RequestException;

import com.bradmcevoy.http.Auth;
import com.bradmcevoy.http.MiltonServlet;

public class LocalConnection implements IConnection {

  // FIXME need to actually implement this method
  @Override
  public HttpResponse doGet ( String url, Auth credentials ) throws RequestException {
    RequestDispatcher dispatcher = MiltonServlet.request ().getRequestDispatcher ( "/REST" );

    ServletRequest request = null;
    ServletResponse response = null;
    try {
      dispatcher.forward ( request, response );
    } catch ( IOException e ) {
      throw new RequestException ( e );
    } catch ( ServletException e ) {
      throw new RequestException ( e );
    }
    return null;
  }

  @Override
  public HttpResponse doHead ( String url, Auth auth ) throws RequestException {
    // TODO Auto-generated method stub
    return null;
  }

}
