package org.xnat.xnatfs.filter;

import org.apache.log4j.Logger;

import com.bradmcevoy.http.Filter;
import com.bradmcevoy.http.FilterChain;
import com.bradmcevoy.http.Request;
import com.bradmcevoy.http.Response;

/**
 * Filter to reduce traffic to the core processing. For example, this filter
 * will 404 requests to /favicon.ico
 * 
 * @author jpaulett
 * 
 */
public class ChatterFilter implements Filter {
  private static final Logger logger = Logger.getLogger ( ChatterFilter.class );

  @Override
  public void process ( FilterChain chain, Request request, Response response ) {
    // Return a 404 and stop processing if the requested path is defined as
    // ignorable
    if ( ignorable ( request.getAbsolutePath () ) ) {
      response.setStatus ( Response.Status.SC_NOT_FOUND );
      logger.debug ( "Ignoring request for " + request.getAbsolutePath () );
      // abort processing
      return;
    }
    chain.process ( request, response );
  }

  private boolean ignorable ( String path ) {
    // If there are more ignorables, move this to an array or list and do a
    // .contains(path)
    return "/favicon.ico".equals ( path );
  }
}
