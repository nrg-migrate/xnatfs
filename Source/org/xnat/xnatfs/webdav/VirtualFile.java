package org.xnat.xnatfs.webdav;

import com.bradmcevoy.http.Auth;
import com.bradmcevoy.http.GetableResource;

abstract public class VirtualFile extends VirtualResource implements GetableResource {
  public VirtualFile ( XNATFS x, String path, String name ) {
    super ( x, path, name );
  }

  // FIXME provide based upon REST API's response
  @Override
  public String getContentType ( String accepts ) {
    return null;
  }

  @Override
  public Long getMaxAgeSeconds ( Auth auth ) {
    return null;
  }

}
