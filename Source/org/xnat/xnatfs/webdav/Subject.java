/**
 * 
 */
package org.xnat.xnatfs.webdav;

import org.apache.log4j.Logger;

/**
 * @author blezek
 * 
 */
public class Subject extends VirtualDirectory {
  final static Logger logger = Logger.getLogger ( Subject.class );

  public Subject ( XNATFS x, String path, String name, String url ) {
    super ( x, path, name, url );
    elementURL = mURL + "experiments?format=json";
    childKeys.add ( "ID" );
    childNames.add ( "label" );
    verboseDirectoryName = "Experiments";
    extraChildren.add ( RESOURCES );
  }

  @Override
  public VirtualResource child ( String childName ) {
    if ( xnatfs.useVerboseFolders () && !verboseDirectory ) {
      // this object is not the verbose folder, but we must create one.
      if ( childName.equals ( verboseDirectoryName ) ) {
        VirtualDirectory d = new Subject ( xnatfs, absolutePath, verboseDirectoryName, mURL );
        d.setVerboseDirectory ( true );
        return d;
      }
    }
    if ( childName.equals ( RESOURCES ) ) {
      VirtualDirectory d = new Resources ( xnatfs, absolutePath, childName, mURL );
      d.setVerboseDirectory ( true );
      return d;
    }
    try {
      getElementList ();
    } catch ( Exception e ) {
      logger.error ( "Failed to cache element list", e );
    }
    if ( childMap.containsKey ( childName ) ) {
      logger.debug ( "child: create " + childName + " as " + childMap.get ( childName ).id );
      VirtualResource experiment = new Experiment ( xnatfs, absolutePath, childMap.get ( childName ).name, mURL + "experiments/" + childMap.get ( childName ).id + "/" );
      return experiment;
    }
    return null;
  }

}
