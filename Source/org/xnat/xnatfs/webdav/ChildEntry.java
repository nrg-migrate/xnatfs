package org.xnat.xnatfs.webdav;

/**
 * Simple class to hold information about a child.
 * 
 * @author blezek
 * 
 */
public class ChildEntry {
  public String key;
  public String id;
  public String name;
  public long contentLength = 0L;

  public ChildEntry () {
    setAll ( "empty" );
  }

  public ChildEntry ( String k ) {
    setAll ( k );
  }

  public ChildEntry ( String k, String i, String n ) {
    key = k;
    id = i;
    name = n;
  }

  public void setAll ( String k ) {
    key = k;
    id = k;
    name = k;
  }
}
