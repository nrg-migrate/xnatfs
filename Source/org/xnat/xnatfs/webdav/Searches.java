package org.xnat.xnatfs.webdav;

import org.apache.log4j.Logger;

public class Searches extends VirtualDirectory {
  private static final Logger logger = Logger.getLogger ( Searches.class );

  public Searches ( XNATFS x, String path, String name, String url ) {
    super ( x, path, name, url );
    elementURL = mURL + "?format=json";
  }

  @Override
  public VirtualResource child ( String childName ) {
    logger.debug ( "child: creating child: " + childName );
    Search search = new Search ( xnatfs, absolutePath, childName, "/search/saved/" + childName );
    return search;
  }
}
