/**
 * 
 */
package org.xnat.xnatfs.webdav;

import org.apache.log4j.Logger;

/**
 * @author blezek
 * 
 */
public class Root extends VirtualDirectory {

  public Root ( XNATFS f, String path, String name, String url ) {
    super ( f, path, name, url );
    verboseDirectoryName = "Projects";
    childKeys.add ( "ID" );
    childKeys.add ( "name" );
    elementURL = mURL + "projects?format=json";
    extraChildren.add ( "Searches" );
    extraChildren.add ( "Upload" );
  }

  private static final Logger logger = Logger.getLogger ( Root.class );

  public VirtualResource child ( String childName ) {
    logger.debug ( "child: Creating child " + childName );

    if ( xnatfs.useVerboseFolders () && !verboseDirectory ) {
      // this object is not the verbose folder, but we must create one.
      if ( childName.equals ( verboseDirectoryName ) ) {
        Root projects = new Root ( xnatfs, absolutePath, verboseDirectoryName, mURL );
        projects.setVerboseDirectory ( true );
        return projects;
      }
      if ( childName.equals ( "Searches" ) ) {
        Searches searches = new Searches ( xnatfs, absolutePath, childName, mURL + "search/saved" );
        return searches;
      }
      return null;
    } else {
      try {
        getElementList ();
      } catch ( Exception e ) {
        logger.error ( "Failed to cache children", e );
      }
      if ( childMap.containsKey ( childName ) ) {
        // Just return the projects
        Project project = new Project ( xnatfs, absolutePath, childName, mURL + "projects/" + childName + "/" );
        return project;
      }
    }
    return null;
  }
}
