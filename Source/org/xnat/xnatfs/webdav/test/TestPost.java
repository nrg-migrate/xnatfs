package org.xnat.xnatfs.webdav.test;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.xnat.xnatfs.connection.PreemptiveAuthorization;

public class TestPost {
  private static final Logger logger = Logger.getLogger ( TestPost.class );

  /**
   * @param args
   * @throws IOException
   * @throws ClientProtocolException
   */
  public static void main ( String[] args ) throws ClientProtocolException, IOException {
    BasicConfigurator.configure ();
    BasicHttpParams params = new BasicHttpParams ();
    ConnManagerParams.setMaxTotalConnections ( params, 100 );
    HttpProtocolParams.setVersion ( params, HttpVersion.HTTP_1_1 );

    // Create and initialize scheme registry
    SchemeRegistry schemeRegistry = new SchemeRegistry ();
    schemeRegistry.register ( new Scheme ( "http", PlainSocketFactory.getSocketFactory (), 80 ) );
    schemeRegistry.register ( new Scheme ( "https", PlainSocketFactory.getSocketFactory (), 80 ) );

    // Create an HttpClient with the ThreadSafeClientConnManager.
    // This connection manager must be used if more than one thread will
    // be using the HttpClient.
    ClientConnectionManager cm = new ThreadSafeClientConnManager ( params, schemeRegistry );
    DefaultHttpClient mClient;
    mClient = new DefaultHttpClient ( cm, params );
    mClient.getCredentialsProvider ().setCredentials ( new AuthScope ( AuthScope.ANY_HOST, AuthScope.ANY_PORT ), new UsernamePasswordCredentials ( "blezek", "throwaway" ) );
    mClient.addRequestInterceptor ( new PreemptiveAuthorization (), 0 );
    // Add as the first request interceptor
    // mClient.addRequestInterceptor ( new PreemptiveAuth (), 0 );
    String url = "http://10.0.0.12:8080/xnat/REST/projects/Uno/subjects/Sample_Patient/experiments/NewSession/scans/1234/files/LatestUpload.txt";
    HttpPost request = new HttpPost ( url );
    /*
    MultipartEntity multi = new MultipartEntity ();
    multi.addPart ( "file", new InputStreamBody ( new ByteArrayInputStream ( "Empty text\n".getBytes () ), "Text.txt" ) );
    request.setEntity ( multi );
    */
    /*
    ByteArrayEntity entity = new ByteArrayEntity ( "Empty text\n".getBytes () );
    entity.setChunked ( true );
    entity.setContentType ( "application/octet-stream" );
    request.setEntity ( entity );
    */
    logger.info ( "Starting post to " + url );
    BasicHttpContext context = new BasicHttpContext ();

    // Generate BASIC scheme object and stick it to the local
    // execution context
    BasicScheme basicAuth = new BasicScheme ();
    context.setAttribute ( "preemptive-auth", basicAuth );
    HttpResponse response = mClient.execute ( request, context );
    logger.info ( "response.getStatusLine().getReasonPhrase(): " + response.getStatusLine ().getReasonPhrase () );
    System.out.println ( "Writing response" );
    response.getEntity ().writeTo ( System.out );
    System.exit ( 0 );
  }
}
