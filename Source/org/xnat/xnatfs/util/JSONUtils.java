package org.xnat.xnatfs.util;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.xnat.xnatfs.asset.IStringAsset;

public class JSONUtils {
  public static JSONObject parseJSON ( IStringAsset asset ) throws JSONException {
    return parseJSON ( asset.getContent () );
  }

  public static JSONObject parseJSON ( String content ) throws JSONException {
    return new JSONObject ( new JSONTokener ( content ) );
  }
}
