package org.xnat.xnatfs.asset;

import java.io.Closeable;

public interface IAsset extends Closeable {
  public boolean found ();

  public boolean authorised ();

  public long getContentLength ();
}
