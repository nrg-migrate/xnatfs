package org.xnat.xnatfs.asset;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.xnat.xnatfs.connection.ConnectionFactory;
import org.xnat.xnatfs.exception.RequestException;

import com.bradmcevoy.http.Auth;

public class AssetProvider implements IAssetProvider {

  @Override
  public IStringAsset getStringAsset ( String url, Auth auth ) throws RequestException {
    try {
      return new HttpResponseStringAsset ( getResponse ( url, auth ) );
    } catch ( IOException e ) {
      throw new RequestException ( e );
    }
  }

  @Override
  public IStreamAsset getStreamAsset ( String url, Auth auth ) throws RequestException {
    return new HttpResponseStreamAsset ( getResponse ( url, auth ) );
  }

  @Override
  public IAsset getAssetHead ( String url, Auth auth ) throws RequestException {
    return new HttpAssetHead (getHead ( url, auth ));
  }

  protected HttpResponse getResponse ( String url, Auth auth ) throws RequestException {
    return ConnectionFactory.getConnection ().doGet ( url, auth );
  }

  protected HttpResponse getHead ( String url, Auth auth ) throws RequestException {
    return ConnectionFactory.getConnection ().doHead ( url, auth );
  }
}
