package org.xnat.xnatfs.asset;

public interface IStringAsset extends IAsset {
  public String getContent ();
}
