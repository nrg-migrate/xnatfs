package org.xnat.xnatfs.asset;

import java.io.IOException;
import java.io.InputStream;

/**
 * Provide access to the asset as a InputStream. The consumer is expected to
 * call IStreamAsset.close() to free the resources.
 * 
 * @author jpaulett
 * 
 */
public interface IStreamAsset extends IAsset {
  public InputStream getContent () throws IOException;
}
