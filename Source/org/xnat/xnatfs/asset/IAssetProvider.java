package org.xnat.xnatfs.asset;

import org.xnat.xnatfs.exception.RequestException;

import com.bradmcevoy.http.Auth;

public interface IAssetProvider {
  public IStringAsset getStringAsset ( String url, Auth auth ) throws RequestException;

  public IStreamAsset getStreamAsset ( String url, Auth auth ) throws RequestException;

  public IAsset getAssetHead ( String url, Auth auth ) throws RequestException;
}
