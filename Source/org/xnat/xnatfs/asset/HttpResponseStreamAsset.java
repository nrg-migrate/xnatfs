package org.xnat.xnatfs.asset;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;

public class HttpResponseStreamAsset extends HttpResponseAsset implements IStreamAsset {

  public HttpResponseStreamAsset ( HttpResponse response ) {
    super ( response );
  }

  @Override
  public InputStream getContent () throws IOException {
    if ( getEntity () != null ) {
      return getEntity ().getContent ();
    }
    return null;
  }

}
