package org.xnat.xnatfs.asset;

import org.apache.http.Header;
import org.apache.http.HttpResponse;

public class HttpAssetHead extends HttpResponseAsset {

  public HttpAssetHead ( HttpResponse response ) {
    super ( response );
  }

  @Override
  public long getContentLength () {
    Header[] headers = getResponse ().getHeaders ( "Content-Length" );
    if ( headers != null && headers.length > 0 ) {
      return Long.valueOf ( headers[0].getValue () );
    }
    return -1L;
  }

}
