package org.xnat.xnatfs.cache;

import org.apache.log4j.Logger;

public class CacheProvider {
  private static final Logger logger = Logger.getLogger ( CacheProvider.class );

  public static String CONTENT_CACHE = "Content";
  public static String RESOURCE_CACHE = "Resource";

  public static ICache createCache ( String cacheName ) {
    logger.debug ( "Creating EhCache instance" );
    return new Cache ( cacheName );
  }

}