package org.xnat.xnatfs.cache;

public interface ICache {
  public Object get ( String key );

  public void put ( String key, Object value );

  public void remove ( String key );
}
