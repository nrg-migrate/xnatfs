package org.xnat.xnatfs.cache;

import java.net.URL;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.log4j.Logger;

public class Cache implements ICache {
  private static final String EHCACHE_FILE = "ehcache.xml";
  private static final Logger logger = Logger.getLogger ( Cache.class );
  private static CacheManager memoryCacheManager;

  private static void checkCreateCache ( String cacheName ) {
    if ( memoryCacheManager.getCache ( cacheName ) == null ) {
      memoryCacheManager.addCache ( cacheName );
    }
  }

  private static void init () {
    if ( memoryCacheManager == null ) {
      URL url = ClassLoader.getSystemResource ( EHCACHE_FILE );

      logger.info ( "Found cache configuration URL: " + url );

      if ( url != null ) {
        logger.debug ( "Configuring cache from " + url );
        memoryCacheManager = CacheManager.create ( url );
      } else {
        logger.debug ( "Configuring cache by default" );
        memoryCacheManager = CacheManager.create ();
      }
    }
  }

  private net.sf.ehcache.Cache cache;

  protected Cache ( String cacheName ) {
    Cache.init ();
    checkCreateCache ( CacheProvider.CONTENT_CACHE );
    checkCreateCache ( cacheName );
    cache = memoryCacheManager.getCache ( cacheName );
  }

  @Override
  public Object get ( String key ) {
    Element e = cache.get ( key );
    if ( e != null ) {
      return e.getObjectValue ();
    }
    return null;
  }

  @Override
  public void put ( String key, Object value ) {
    cache.put ( new Element ( key, value ) );
  }

  @Override
  public void remove ( String key ) {
    cache.remove ( key );
  }
}
