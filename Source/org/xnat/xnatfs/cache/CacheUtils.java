package org.xnat.xnatfs.cache;

import org.apache.commons.codec.digest.DigestUtils;

import com.bradmcevoy.http.Auth;

public class CacheUtils {

  private CacheUtils () {

  }

  public static String createKey ( Auth credentials, String type, String key ) {
    String hash = DigestUtils.shaHex ( "xnatfs" + credentials.getUser () + credentials.getPassword () );
    return hash + "::" + type + "::" + key;
  }
}
