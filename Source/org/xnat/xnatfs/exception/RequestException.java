package org.xnat.xnatfs.exception;

public class RequestException extends Exception {

  private static final long serialVersionUID = -4169286470059105419L;

  public RequestException ( Exception e ) {
    super ( e );
  }

  public RequestException ( String message, Exception e ) {
    super ( message, e );
  }

  public RequestException ( String message ) {
    super ( message );
  }
}
